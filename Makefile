postgres:
	docker-compose up -d

createdb:
	docker exec -it masterclass-go_teste-postgres-compose_1 createdb --username=golang_study --owner=golang_study masterclass_db

droptable:
	docker exec -it masterclass-go_teste-postgres-compose_1 dropdb -U golang_study masterclass_db

migrateup:
	migrate -path db/migration -database "postgresql://golang_study:golang_study@localhost:5433/masterclass_db?sslmode=disable" -verbose up

migrateup1:
	migrate -path db/migration -database "postgresql://golang_study:golang_study@localhost:5433/masterclass_db?sslmode=disable" -verbose up 1

migratedown:
	migrate -path db/migration -database "postgresql://golang_study:golang_study@localhost:5433/masterclass_db?sslmode=disable" -verbose down

migratedown1:
	migrate -path db/migration -database "postgresql://golang_study:golang_study@localhost:5433/masterclass_db?sslmode=disable" -verbose down 1

test:
	go test -v -cover ./...

sqlc:
	sqlc generate

mock:
	mockgen  -package mockdb -destination db/mock/store.go masterclass/db/sqlc Store

server:
	go run main.go
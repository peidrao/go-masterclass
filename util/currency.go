package util

const (
	USD = "USD"
	BRL = "BRL"
	EUR = "EUR"
	CAD = "CAD"
)

func IsSupportedCurrency(currency string) bool {
	switch currency {
	case USD, BRL, EUR, CAD:
		return true
	}
	return false
}
